import { IonGrid, IonFab, IonFabButton, IonRow, IonCol, IonImg , IonHeader, IonPage, IonTitle, IonToolbar, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonIcon, IonList, IonItem, IonLabel, IonInput, IonToggle, IonToast, IonButton, IonRadio, IonCheckbox, IonItemSliding, IonItemOption, IonItemOptions, IonContent } from '@ionic/react';
import { add, settings, share, person, arrowForwardCircle, arrowBackCircle, arrowUpCircle, logoVimeo, logoFacebook, logoInstagram, logoTwitter } from 'ionicons/icons';
import React, { useState } from 'react';
import './ListPage.css';

interface ContainerProps { }
interface ILocalisation{
    adresse?: string;
    city?: string;
    postalCode?: string;
}

interface IAppartement{
    id: number;
    name?: string;
    cols?: string;
    picture?: string;
    price?: number;
    rent?: number;
    adress?: ILocalisation;
    rate?: number;
    created_at?: string;
}

function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

let biens: IAppartement[] = [
    {
        id: 1,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "https://cdn-eu.green-acres.com/3512a/3512a-wsud3802-nk/miniPhotos/3512a-wsud3802-nk_1.jpg?637308238250000000",
        price: 500,
        adress: {
            adresse: "4 rue du caramel",
            city: "CaramelCity",
            postalCode: "000001"
        },
        rate: 1,
        rent: 1,
        created_at: "2020-10-01",
    },
    {
        id: 2,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "https://cdn-eu.green-acres.com/3512a/3512a-wsud3802-nk/miniPhotos/3512a-wsud3802-nk_1.jpg?637308238250000000",
        price: 500,
        adress: {
            adresse: "4 rue du caramel",
            city: "CaramelCity",
            postalCode: "000001"
        },
        rate: 1,
        rent: 1,
        created_at: "2020-10-01",
    },
    {
        id: 3,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "https://cdn-eu.green-acres.com/3512a/3512a-wsud3802-nk/miniPhotos/3512a-wsud3802-nk_1.jpg?637308238250000000",
        price: 500,
        adress: {
            adresse: "4 rue du caramel",
            city: "CaramelCity",
            postalCode: "000001"
        },
        rate: 1,
        rent: 1,
        created_at: "2020-10-01",
    },
    {
        id: 4,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "https://cdn-eu.green-acres.com/3512a/3512a-wsud3802-nk/miniPhotos/3512a-wsud3802-nk_1.jpg?637308238250000000",
        price: 500,
        adress: {
            adresse: "4 rue du caramel",
            city: "CaramelCity",
            postalCode: "000001"
        },
        rate: 1,
        rent: 1,
        created_at: "2020-10-01",
    },
    {
        id: 5,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "https://cdn-eu.green-acres.com/3512a/3512a-wsud3802-nk/miniPhotos/3512a-wsud3802-nk_1.jpg?637308238250000000",
        price: 500,
        adress: {
            adresse: "4 rue du caramel",
            city: "CaramelCity",
            postalCode: "000001"
        },
        rate: 1,
        rent: 1,
        created_at: "2020-10-01",
    },

];

function getColSize (): string{
    return '4';
}




const ListPage: React.FC<ContainerProps> = () => {

    function selectBien(id: number){
        let biensSearch: IAppartement[] = biens.filter((bien: IAppartement) => bien.id == id);
        let bien: IAppartement | null = (biensSearch[0] != null) ? biensSearch[0] : null;

        console.log(bien);
    }

    return (
        <IonContent>
            <IonGrid>
                <IonRow>
                    {
                        biens.map((el: IAppartement) => <IonCol size-md={ el.cols } size-sm="6" size-xs="12">
                            <IonCard onClick={() => selectBien(el.id)}>
                                <IonCardHeader>
                                    <IonImg src={el.picture}/>
                                    <IonCardSubtitle>{ el.adress?.adresse } - { el.adress?.city } - { el.adress?.postalCode }</IonCardSubtitle>
                                    <IonCardTitle>{ el.name }</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    Rate : { el.rate } <br/>
                                    Created: { el.created_at }
                                </IonCardContent>
                            </IonCard>
                        </IonCol>)
                    }
                </IonRow>
            </IonGrid>
            <IonFab horizontal="end" slot="fixed" >
                <IonFabButton>
                    <IonImg src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAEaElEQVRoge2a228VVRTGv6GNPaRVaOspNWoi0WDwUn3UEDSaqAfLgy+agPqGQkwQovwXxhcSjPp/GCJGEAVCuSmaSFuKDxwkYqJGhEpF8/Nhr83ZTXvmsmdGS8J62cnMWt+31r6uWXukW7K0JKkKCBiS9JSkdZIeknS/pBFJ/aZyVdIlST9I+l7SIUlfJknyW1U+RAvQAF4H9gH/UFz+Bj4FXgP6yvgSNSLAcklvSXpX0l32eE7SEUkHJH0naUrST5Ku2PsBSaOSHpQ0JukZSU9K8gFclPS+pA+SJLkW41fRIMaBc0GvHge2ACsisFYCbwAnArwZYEMdvnvSBvBhQHgSeL5C/BbwdYC/B2hUhe9JRs1xgKvAdqCnUhLH0wPsAGaD0V5VFfhqG26AM8AjlQCnc44BU8Z5FlhdFrAZAB4D7qzI1zzcg8Ah4z4HjMYCNYLpdAToz7aqVoB+4GgwzYqvmWBhn8EddnntWkCb7nIeaBXAGw5mxZ6iQYwHC7vQmjBHs+R8QcwxOhtAvk4AltM5J7YXITR7AGLfp9jtNNOzuaYYsMsMThKxxdYYSC/wjZnvzFLuA3405eeKkuVxNDYQs33RzC+mjgoueQM4HkOUx9GSgSR0dtLNaYqfmdKWGKI8jpYJxOy3GsTebgpDuLT6GhEJYF5HKwhkEJgDrod+Lgt0npbUI+lwkiS/xxLVLfYhdlRSr9yHnKT5gayz9otuIGQfdrl7Og2D7EPzgLXrFwtkrbXfpgB8LOmevM6WkHuNq5uctnbtgjfAtPXGwpcdnVLzO6/kWGcPm8qkfxaOyLC1l+pysELxPt7IAcNABqy9oqUvf1h7h3+wrIviTSdhIGG1Y6mLH4nL/kEYyC/WVvONXK+MWPurfxAGMm3tmhSAtpR5BuQ62XPYtlP88D56n+cF4reyx1IA3swgqEraxtVNvI+TC94AL1mH7C/jQd4RKclx0GA2LvZykE7SuLIESa2B4JJbnzQu3H4tGftcrhb7cizRfyCvSLpN0r4kSS4vqgG8ah12IpalzhHBfVidMohNaYp9wAVTfCGSrM5ANpp5m6xrCOAdUz7F0is+nDbzt/MYNOjUendEENYViO/gqczRCIw2mNEsMFaQsI4C3ePAn2Zb7CoDdz/he2A42+KGXSsjmKIl0yauKAewu1AQBtDAFY7BFZL/jyL2ADBhPkzknlKLADWBSQM6BjQr9jWNewg4bNwzlL3wwV30+KGdAtJysUrE1oTnnAbuqwp4VTDNZnEF5d5KwOfz9OJ2J7+wJ4CRbMtiJI1gAwBXUK7k9hV3Yo/TOScAdkeviZykrWDYwdVitwKDEVhDwDY6aYefSoVvi2N/GGhI2iZpl6S77fFfchXA/XK1sWm5HwZ8oeB2uZ8L1kh6VNKzkp6QSwAl6YKk9yR9lCTJXIxf0YLLzTYDe3GfAEXlOvAJsKnsNKryp5oVcrXY9XIVwAckNeVGQnIj87OkGbkvu68kHeyait+Sm1z+BU8zkd9FY91dAAAAAElFTkSuQmCC"/>
                </IonFabButton>
            </IonFab>
        </IonContent>
    );
};

export default ListPage;
