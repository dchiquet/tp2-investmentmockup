import {
    IonGrid,
    IonFab,
    IonFabButton,
    IonRow,
    IonCol,
    IonImg,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCardContent,
    IonIcon,
    IonList,
    IonItem,
    IonLabel,
    IonInput,
    IonToggle,
    IonToast,
    IonButton,
    IonRadio,
    IonCheckbox,
    IonItemSliding,
    IonItemOption,
    IonItemOptions,
    IonContent,
    IonAlert
} from '@ionic/react';
import { add, settings, share, person, arrowForwardCircle, arrowBackCircle, arrowUpCircle, logoVimeo, logoFacebook, logoInstagram, logoTwitter } from 'ionicons/icons';
import React, { useState } from 'react';
import img_profil from '../images/profil.png';
import img_edit from '../images/edit-icon.png';

 import './Users.css';

interface ContainerProps { }

interface IProfil{
    id: number;
    name?: string;
    cols?: string;
    picture?: string;
    apport?: number;
    loan_rate?: number;
    insurance_rate?: number;
    duration_loan?: number;
    notary_fees?: number;
}

/*
function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}


let profilactif: IProfil[] = [
    {

        id: 1,
        name: "Les jolies maisons",
        cols: getColSize(),
        picture: "../images/profil.png",
        apport: 500,
        loan_rate: 25,
        insurance_rate: 15,
        duration_loan: 15,
        notary_fees: 5,
    },
];


function getColSize (): string{
    return '4';
}
*/




const Users: React.FC<ContainerProps> = () => {

    const [showAlert1, setShowAlert1] = useState(false);


    return (
        <IonContent>
            <IonAlert
                isOpen={showAlert1}
                onDidDismiss={() => setShowAlert1(false)}
                cssClass='my-custom-class'
                inputs={[
                    {
                        name: 'name',
                        type: 'text',
                        value: 'PASCAL Pierre',
                        placeholder: 'Your Name',
                    },

/*
                        Apport: 500 €<br/>
                        Loan rate: 25%<br/>
                        Insurance rate: 15%<br/>
                        Duration loan: 15 years<br/>
                        notary_fees: 5%<br/>

*/
                    {
                        name: 'picture',
                        type: 'url',
                        id: 'name2-id',
                        value: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA/FBMVEX///+9gWmpfFDt2bTcxaGAYEs0IhS8n4Liy6fkz6q5eF3ex6O6e2G/gmq8fmakc0HawZqmd0h6WUXs1q6ldUX04Lq/hW14VkK1hmR3UzqpflL59PL17er7+ffj3dl6WEGTalTu4dzLno0dAAAWAADQq4zRuJbk18zIvLWegmiWeWGojXKgcVvHlYLSrJ4hCQDhycCKa1XDp4nauJfIrZazjGevg1zPuKTVwbCsmY6fiXvBtKzgxr3Ws6ZKOSmlkXXJnYD06NPEkHXm1r26mHjb1M+ploqPdGPCta6RYkmsd2A7Khutm39xX0qMe2PBro8pFQRhUT9TQjHy48mgoxe5AAAPNElEQVR4nO2daXvaxhqGLcBGIECWjCAOENdxAl7SxFCDydraoctJ25DT/v//cma0jmZG0jsLiJyL55MvGzS69a6zgA8O9tprr7322muvvfYCavDs18vL92+x3iNd/vpsWPYtadPo2eVvv58inZHCv5i+e3v5rOzbU9Xwj9/PENohXy4idd+9/34pR5e/n2bBEUL2fHc5KPtmJTR8C8GLjHn67teyb1hUAnwB5On0sux7FtGzQzE+X2dn3w/j5ak4H9bp9DvJOrKAmPGPsm++WJfvZDw0QfytbIAC/YEqnALfYVA8djevSiUYVu7OGlIh/iidvSubhav32gBROL4vm4YjfRb0EXdv7vFMK+Chu3t+OtUKuING1OujSGdvy0aipJkPI5aNlNalUiHkNgmnu1X3OVHoIkHw+odXV4ecl57tVNlnE6l7NauNZ9eFjP3rsYdk3nAQy6Yi9ZZyUnc6Ni0kc3yVy9i/rnlVX16DeRqnuzSVopzUvbKsmi/EeN3P4nPddcjnM9apF57tUF8zTDupe2XWYlnmesq3Y/+mSgAixHUacZeKPpVJr60aKcuqc7KlO62l+DiIh6OywUKNPv4nfWO1NCFmvKEY3cMZzYcRZylEt/mxbDZfH1qt9M2PaUDMWLshCoLrUg4aI6ZT6peWcVs23sGk1zKMFOCNyQLicKzVp30X0/WndT4fVipnvWobzd55qXyD503DaKcID3l8gata4/rV9c3MyuarVk3ST39oG0an+brEcPzQ7BhIX0gT1jk+SkBajRw87Kd1N02IGFtluergxya+AaP9ijRhDqAvMxcQIZJ+6hMaRvN5KWactDoGQ5hrQhBhtUb4qRGq0ykhGl83o+FTNiwCLCb0iEbPaEeDbL1wDHEKjQh/IBKpOmHVSoz4JR7EaP24VU+dNDsGl5BXC0UJCSMShCjhbNFTPzaJkUnCa24tFCQkjPilTQ7U/LAtwOctg0/ozgpNCCH0rqMLvkoRGs3XW+EbPaYBSRsWA0IIq+M+nxAF4xYAh0bHyCK8KnZSEGFcE2lCo9Xb+I7/eYsGTAghTgojnIW55geaED3eDa+jnjfpIUkbFvPBCKuNfhYhSqkbReQBJoSATAok9K4yCTeLyAWMe5rijg1MWF27mYSbROQDJoTF5R5MWO1nE24OcdjiDZcQTiGAQMKwJPIJjU5vIx3coMNk0ZDwC7xWgAmDbJpBiBA3QdjLAIxmwLAwhHppMIfKItxI6f8xEzBcxYCFIZSwmuelGFF7A/c6Iwh9BV4KclIoYRCI2YTa2/AP/DQaGhFeDeGE/noN07WRiFonUxl1giQETH6FvHRdRGg0Nbaoozy+sKnhJBqLxwwl9FNNLmFHY7bJzjIB4Ste221a47HJMrKEnmeaHrPG6LemX3LH1ZdtPuRlGSMsiHQqNa/+vHjz6S8mOBlC768/Ly4+rZltmmJCoznRA5gfhFg+YXo/xvz74gjpxc80Ik3ofX0RvJBC9HAcGnleiq2oJxSzS30sZupk/ewDHh29oaskTbh+EbzwRY0ixNcsGldPKH4s8FGk9pS2ofkpuO+jb1/NXELvv9/CF/5NGRF7aYEJkZ9qWPAv9lGj8xJN5/qksaxxaMKjo3/yCasvohd+Sv8e59Kcgh9Jg58CfLSHT1K4az7hUT5hIyb8lv4Dar3dm17x032uCgjw0c5LC98OWQ+tWkz4qcBLk0eR/j1+aLOXxY9XNZ8Oin3U6CD/dKmeJonDv+XiEC+3ueOfigkNxYnUc8AQHXTbU2p6aP78Jrjxi3V+LvXWoRFfrNPei8Kw3zABw7eU9mwAaQbJQuUdTfFTxjL/8W1zQadSth4GhfPiazqV+m1ptQEgVEs2Be1aTGihmUC6XFjW1zdvLo6Ygs/pab5+u7j4RhV8/8jCtQcjVGjeJiATYsKxSzemllmbrS12RsXpS6vjMb217/m9PIzQaMovTAEqheHHoVXjrNNYCnMLXA3dsWfBbkC6YtzCTNj5yfJTjcucFOKpAQH0nbTf8CC51FAwYnG9DQhfIje9cYFLUSBCf5Vm6lV/gRHKGhEYhYbxiAjx6t8Uso4BIsTba+6NVwU+Y1kjwhKp4QdizQ8cyN4ThNB3UnftmcUNVXgHUukUVgv96/8SBCLIiBDCRh+r4QGd1JBcs4G0M5FqfiAe9gFGBBGOayZ6HaxW+JJpbCAdaSRkRGs8vb66AeQaWKbxfRVuQnQL4oSASQVx/WPLMk2TWwClCWHFMFRLfIohcHWkHmyxVIhQ6AbECwa4VIQDPEIRwYSwhiqWcK4RyTMBol7ChiCg0RLcxxiJmdCAWxFICJjdU+MLzoRvRfJMIGAowgi9R+HxBfsaUSc1ws5Gmw3FxxcrieJOGjTg2giFKkU4/qMIoWAmDUb4RSOhcBgagm76WmIAYCDC5oePEjcgdOBdgg8aiDAbyjxhkaI/lHBSaCCCVjFqMoQivWnRhmHGAKBABBHKhKHQzr5ErfCli9CDzu3TEmhrpEwYLEkVChKHkKVu3vjgQJQLw1w3PY5lnsTS7KTIiFBCiZYtUGa9OB6MGB0MsxAlWrZA4IooNPklle2mnznD6HZSgYoIXmRjCDPd9JhNcw+ZJpR1UnhrKnl9I6/oH9MOlAkoOLlPDQ9MNbKJxsjPNQ9AQHkTgndLz2XD0MgtGMefk3WGQTXbgtJRiNSCnR2WTqW+crZnjs2H4ehgNHjI4VPwUQPc1UinUl+9TMCaXxYbOXUQS2ZWkRDC1hRle7ZARes1BT2NEiC0b1MbpAgxl1BomZtHCCsX8qkUgphLqPhwoeVClTB/7TSPUBUQeJJPZEsma6BedkbNIVQGBBZEhYKfIBqZH03IJDRFV7l5akII4Tujecos/RmEwGMXRdoiYWYDxyf0XioV4VhNSFMjs1bKE0qpPEY+oWKViAUj1PM0cb7hBSOPUHijKVOgPTaVxjutzoqDyDn1VVvpAoTN8jXFoYFDsT5jygZLOKtrJIS03hoJX47r9aKz+rV6va4wI6S0dUILIVJmTBM2ZgiwATy+BhCIUEfFD4QIMWJ9TDKmCPFf6+ADegBtn7DmIyJXtTiE2EERYLW6ZUINfWkof6vGCjhixpiwhh20PsM/aiSE5FKZ/V++gs0oK0DBvmolhL5pQ0CdhKAzJ9oIW7eB2axZiIMhG161ETIj6uDw82dtJRjU0+gjbB48HAeIocUw5WwW/1wP7HnyoC8wQJ039GhwofB09HOIWCO4Io3DgPz34ECbl7ZBhNKL+pT8RZPoxCnDOI4b1APVxa9EwE/rSR1T4MjP3KO4EKYYE74qjhy5TWdWwHUatfXSRMEC9PA4RkT1f4zicEzgVU/85KerjwKutamteceKnudDjOhDpr838STYzRjoeqiw7TUtD7TdezILr/eZQKTnh/8GLxk5T4o/TAkQ8CvdNORuxPfUmUcXNLMJo4dqV54+6alDQjeBlQfqPalgRdcbZRJG9XnSRS/XwQgDVC0XvYXPV7HjDmp4zCU8iZ/4shu8ZaHGCP5ot0q5aD8J+RBhEhREtkkIT5I90zsnepMSI/iDJfLJtJfwVSpdYqsryTYJ4b/Jn1fJuyoL+aQDPqkgmUxxeiHutOLcE9c0WULir6n3IUbJxhF+7EuGkOZDhDPikiOa8IScBXTT75RNOrC+G0s81bB8SHPymlG2adBZBmlgM++VYRT4DgnRvo3Lh4yYumiYbRp0ljnwyyErcUaBk95igZjBR5YLX0G2adBZ5iAsh+qMAqcvRwI2zORLlQtfZkKY/sPSybiCGCPwrIkvcCC2w/aFqy51MmIUEZ5QtzLLIhRiFPoqF+B8Lcd+PuGSuizONg0qy2Ctci6CGWGEQp8Lgq2Z5vOhTHNHXxdlmwaVZbAWuZdBjG2IHcU+NAN4bEV8iHDFXPfzcYPKMli8VEoxFt+P2CdKAPWikA9pwV7YbFSZ3w0LCRFj4TMX/PBaQb1oPxY4ViCbvfCowSY8bjlkH1dByhH9xHreI2v3QHyIkDMonX2QbvnlUIxR+As/c9y0OABjQvYg3b1dZ3+XXSzSykurop+wzF7KaIP5OOUCAVZsJsPmlEOGMdOM4p/I5xd9sIP6YsrFEkecfU/9di5wySxXlfhqDO6RDLiDBqLKxTJIKTSi0CUrFe4MWea7PlkbihkQ6ykPECGmvHcETDSxeGaU+T4sJtfktaAZcsjSsEyKQgoRUg7TYhsA4TyDReWadk/MQQMSolwsSRAyy06ECVkzikwrEqWX3AQjkAFZ2pl/EfVSrLQZJb/RjGy/JTwUKykXS9pQCSK4HKa1IPOM5BeaJbt6Mh6KFS+33bKeGM+P63KERKsq9wU8hBHbj5KAFSdsYLihFiEKlcM0YlvNhLER5TzU1zwbEPlwkIfkr14JglHhK+n8dNpWAKx0cwCR8LMfSaTSWH71hx3A4AvXRBVAf7ktG9CpjGTKIaknil8NOWoZPZXxcazllTtnMZIqh4R64C01vj60RBu1tLqT/Omts5Arh4kWqv878FEylUdaFbzfWd2pjeCILc+wgq0wqNyh4iOkl53FpfiINy12xVJcZTMUSB1QNddtVpyVIAkJLKNsW6lNWAXJdqWb19Pimwdp4/lUVup5NNL9biLanNVlWRXV7VLE2fiR16hsGq40Au5kKPI2RVS0c1VRTyUktWPZRmeWiTRTm+XoVVdDO8pqhxJqV2caJTTfFUStdWIXEZ158a3KarELiJsEPBjtgBU3CniwA466qSSTaFVu0eiyBx20q15m6bc1TXnzVWJ3wxxy2JCYvcCtAWrvRbN0XkosOhWt/1s1X4P59hm78438q+pM3W3bU7cVgokmqsvxQnIqWwvBRIP59sxor7broZGW9nbM6DglGDDQYLUNM9orjf+8WVi3G4/GbhkRSGp0t1FXdbafQlkNN+eqjl3XvGQoqcmGsqo932ITU6DJQjujYy9KDkBKyI4649Gx57vFhzVZaWNE8bc7/klqeFfR4KyO/fS+nA4GJGRItVlH167vnnumNVjO7a6cuzpde7XcYfMlGixXXdGYdBzbqd+W2Z6JanI/t21gS4dsZ6/udzO35Go0Wa6ednNd1kFw3UV9ef5d+CZfg/Pl3WphI3W7XScS+hn/arG6v/2e4UgNhueT5fL+/g7r/n65nJwP/0/Q9tprr7322muvvbah/wHVyf04hi80cgAAAABJRU5ErkJggg==',
                        placeholder: 'Your Picture',
                    },
                    {
                        name: 'apport',
                        value: 500,
                        type: 'number',
                        placeholder: 'Apport (€)',
                    },
                    {
                        name: 'loan_rate',
                        type: 'number',
                        value: 25,
                        min: 0,
                        max: 100,
                        placeholder: 'Loan Rate (%)',
                    },
                    {
                        name: 'insurance_rate',
                        type: 'number',
                        value: 15,
                        min: 0,
                        max: 100,
                        placeholder: 'Insurance Rate (%)',
                    },
                    {
                        name: 'duration_loan',
                        type: 'number',
                        value: 15,
                        placeholder: 'Duration Loan (Year)',
                    },
                    {
                        name: 'notary_fees',
                        value: 5,
                        type: 'number',
                        placeholder: 'Notary fees (%)',

                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: () => {
                            console.log('Confirm Ok');
                        }
                    }
                ]}
            />

            <IonGrid>
                <IonCard
                    // onClick={() => }
                >
                    <IonCardHeader>
                        <IonGrid>
                            <IonRow>
                                <IonCol size="12" size-md="4" size-sm="6" size-xs="12">
                                    <IonImg src={img_profil}/>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                        <IonCardTitle>PASCAL Pierre</IonCardTitle>
                        {/*<IonCardSubtitle></IonCardSubtitle>*/}
                    </IonCardHeader>

                    <IonCardContent>
                        Apport: 500 €<br/>
                        Loan rate: 25%<br/>
                        Insurance rate: 15%<br/>
                        Duration loan: 15 years<br/>
                        Notary fees: 5%<br/>

                    </IonCardContent>
                </IonCard>
            </IonGrid>


            <IonFab horizontal="end" slot="fixed">
                <IonFabButton onClick={() => setShowAlert1(true)} >
                    <IonImg src={img_edit}/>
                </IonFabButton>
            </IonFab>
        </IonContent>
    );
};

export default Users;
